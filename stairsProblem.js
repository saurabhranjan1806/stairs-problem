/**
 * Problem is to find the number of ways in which you can
 * climb N stairs when at a time you can take either 1 step or 2 steps.
 */

/**
 * A simple recursive solution is below
 */

 /**
  * recursive solution
  * @param {int} n - Number of stairs to climb,
  * returns number of ways you can climb those stairs
  * 
  * when n = 1, number of ways = 1 as you can take 1 step to reach
  */
const recursiveStairsSolution = (n) => {
  if (n <= 1) {
    return 1;
  }
  
  /**
   * for n > 1 we can find the number of ways as
   * sum of ways to reach n - 1 stair + n - 2 stair.
   */
  return recursiveStairsSolution(n-1) + recursiveStairsSolution(n-2);
}
// 1 1 2 3 5 8 13 21
console.log(recursiveStairsSolution(7));
/**
 * this solution O(2^n) time to complete as each problem dives into 
 * 2 subproblem and each divides in another set of 2 subproblems.
 * 
 * it takes no extra space based on the input so
 * space complexity is O(1).
 */




 /**
  * Another solution to this problem is using Dynamic Programming.
  * As while solving the recursion tree there are multiple overlaps.
  * We can use both top down and bottom up approach to solve this.
  * 
  * Below is the top down approach with a little modification to
  * the above recursion problem.
  */

 /**
  * DP solution
  * @param {int} n - Number of stairs to climb,
  * returns number of ways you can climb those stairs
  * 
  * when n = 1, number of ways = 1 as you can take 1 step to reach
  */
const DPStairsSolution = (n) => {
  const memorizationObject = {};
  if (n <= 1) {
    memorizationObject[n] = 1;
    return memorizationObject[n];
  }
  
  /**
   * for n > 1 we can find the number of ways as
   * sum of ways to reach n - 1 stair + n - 2 stair.
   */
  if (memorizationObject[n]){
    return memorizationObject[n];
  }
  memorizationObject[n] = DPStairsSolution(n - 1) + DPStairsSolution(n - 2);
  return memorizationObject[n];
}
// 1 1 2 3 5 8 13 21
console.log(DPStairsSolution(7));
/**
 * this solution O(n) time to complete as each problem is only
 * solved exactly once and if its solved once its stored and returned
 * from there if needed again.
 * 
 * it takes N extra space to store solution to each n subproblems so
 * space complexity is O(n).
 */





 /**
  * Another solution to this problem is via sliding window technique
  * As when we have to find the number of ways to reach a stair
  * it's the sum of the ways to reach last two stairs.
  * 
  * Therefore we can maintain the the sum in a variable while
  * approching the problem bottom up.
  */

 /**
  * bottom up solution
  * @param {int} n - Number of stairs to climb,
  * returns number of ways you can climb those stairs
  */
const BottomUpStairsProblem = (n) => {
  let sum = 1;
  let prev = 1;
  let prevprev = 1;

  for (let i = 2; i <= n; i += 1) {
    sum = prev + prevprev;
    prevprev = prev;
    prev = sum;
  }

  return sum;
}
// 1 1 2 3 5 8 13 21
console.log(BottomUpStairsProblem(7));
/**
 * this solution O(n) time to complete as we loop the array once.
 * 
 * it takes constant extra space to store sum and prev and prevprev
 * variables. Therefore
 * space complexity is O(1)
 */
